package me.mrkamen.grant;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public final class Grant extends JavaPlugin implements Listener {
    private static Map<String, Map<String, Integer>> giveGroups = new HashMap<>();
    private static Map<String, Group> groups = new HashMap<>();
    private static Permission perms = null;
    private static FileConfiguration config;
    private SQLConnection sqlConnection;

    private void setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
    }

    public static Permission getPermissions() {
        return perms;
    }

    public static Map<String, Map<String, Integer>> getGiveGroups() {
        return giveGroups;
    }

    public static Map<String, Group> getGroups() {
        return groups;
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();
        setupPermissions();
        config = getConfig();
        config.getConfigurationSection("groups").getKeys(false).forEach(group ->
                groups.put(group, new Group(config.getString("groups." + group + ".name"),
                        config.getString("groups." + group + ".item"),
                        config.getInt("groups." + group + ".priority")
                )));

        config.getConfigurationSection("give_groups").getKeys(false).forEach(group -> {
            Map<String, Integer> groupGiveLimit = new HashMap<>();
            config.getConfigurationSection("give_groups." + group).getKeys(false).forEach(group2 -> {
                groupGiveLimit.put(group2, config.getInt("give_groups." + group + "." + group2));
            });
            giveGroups.put(group, groupGiveLimit);
        });

        try {
            sqlConnection = new SQLConnection(
                    config.getString("mysql.host"),
                    config.getString("mysql.username"),
                    config.getString("mysql.password"),
                    config.getString("mysql.database"),
                    config.getInt("mysql.port"),
                    this);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        getCommand("grant").setExecutor(new GrantCommand(sqlConnection));
        getCommand("grantinfo").setExecutor(new GrantInfoCommand(sqlConnection));
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        sqlConnection.close();
    }

    Map<String, String> activePlayer = new HashMap<>(); //храню ники игроков и донат, которые должны ввести в чат имя игрока для выдачи доната

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        InventoryHolder grantInventory = e.getInventory().getHolder();
        int slot = e.getRawSlot();
        Player player = (Player) e.getWhoClicked();
        if (!(grantInventory instanceof GrantInventory))
            return;

        Map<String, Integer> allowGiveGroup = ((GrantInventory) grantInventory).getAllowGiveGroup();
        Map<Integer, String> inventoryGiveGroup = ((GrantInventory) grantInventory).getInventoryGiveGroup();


        if (allowGiveGroup == null || allowGiveGroup.isEmpty()) {
            e.setCancelled(true);
            player.closeInventory();
            return;
        }

        if (slot >= 19 && slot < 19 + allowGiveGroup.size()) {
            activePlayer.put(player.getName(), inventoryGiveGroup.get(slot));
            e.setCancelled(true);
            player.closeInventory();
            player.sendMessage(
                    "Напиши в чат ник игрока, которому хочешь выдать привилегию %group"
                            .replaceAll("%group",
                                    groups.get(inventoryGiveGroup.get(slot)).getName()
                            )
            );
        } else if (slot >= 0)
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void chatEvent(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();
        String playerName = player.getName();
        if (!activePlayer.containsKey(playerName))
            return;

        String groupName = activePlayer.get(playerName);
        Player playerExact = Bukkit.getPlayerExact(e.getMessage());

        if (playerExact == null) {
            player.sendMessage("§fДанный §eигрок §fсейчас §cоффлайн§f, попробуйте снова, когда он будет §aв сети");
        } else if (getGroups().get(getPermissions().getPrimaryGroup(playerExact)).getPriority()
                < getGroups().get(groupName).getPriority()) {
            Bukkit.getScheduler().runTask(this, () -> {
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                        config.getString("command")
                                .replaceAll("%group", groupName)
                                .replaceAll("%player", playerExact.getName())
                );
                try {
                    sqlConnection.saveGivePlayerGroup(playerName, playerExact.getName(), groupName);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                player.sendMessage("Привилегия %group успешно выдана".replaceAll("%group", groupName));
            });
        } else {
            player.sendMessage("§fУ данного игрока §cгруппа выше§f, чем та, которую вы пытаетесь выдать");
        }
        activePlayer.remove(playerName);
        e.setCancelled(true);
    }

    @EventHandler
    public void leavePlayer(PlayerQuitEvent e) {
        activePlayer.remove(e.getPlayer().getName());
    }
}
