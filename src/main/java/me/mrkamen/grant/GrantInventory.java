package me.mrkamen.grant;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class GrantInventory implements InventoryHolder {
    private Inventory inv = Bukkit.createInventory(this, 45, "§e§lВыдача привилегии");
    private Map<String, Integer> allowGiveGroup = null; // группы, которые игрок может выдать, null если игрок группы выдавать не может
    private Map<Integer, String> inventoryGiveGroup = new HashMap<>(); //послотовок хранение групп в инвентаре, которые игрок может выдать

    public GrantInventory(Player player, Map<String, Integer> groups) {
        allowGiveGroup = allowGiveGroup(player, groups);
        createInfoBook(allowGiveGroup);

        List<ItemStack> items = new ArrayList<>();
        if (allowGiveGroup != null && !allowGiveGroup.isEmpty())
            allowGiveGroup.forEach((group, number) -> {
                Group groupNow = Grant.getGroups().get(group);
                items.add(createItem(
                        groupNow.getMaterial(),
                        groupNow.getName(),
                        "§fЧтобы выдать данную привилегию, ",
                        "§eНажмите §fна данный предмет",
                        "§fИ §eнапишите никнейм §fв чат",
                        "",
                        "§fМожете выдать: §c%number §f".replaceAll("%number", number.toString())
                        + (number > 1 && number < 5 || number > 20
                                && (number % 10 == 2 || number % 10 == 3 || number % 10 == 4)
                                ? "раза" : "раз")
                        )
                );
                inventoryGiveGroup.put(inventoryGiveGroup.size() + 19, group);
            });

        for (int i = 19; i < items.size() + 19; i++) {
            inv.setItem(i, items.get(i - 19));
        }
    }

    private void createInfoBook(Map<String, Integer> groups) {
        ItemStack book = new ItemStack(Material.BOOK_AND_QUILL);
        ItemMeta itemMeta = book.getItemMeta();
        itemMeta.setDisplayName("§eВыдача привилегии");
        List<String> lore = new ArrayList<>();
        if (groups == null) {
            lore.add("§fДля выдачи привилегии §c§nдругим игрокам");
            lore.add("§fПриобретите группу §bOP §fили выше");
            book.setItemMeta(itemMeta);
            itemMeta.setLore(lore);
            book.setItemMeta(itemMeta);
            inv.setItem(2 * 9 + 5 - 1, book);
        } else if (groups.isEmpty()) {
            lore.add("§fВы §cуже выдали §fвсе группы, которые могли");
            lore.add("");
            lore.add("§fДля выдачи группы, §eкупите привилегию выше");
            lore.add("§fИли купите её своему другу на нашем сайте");
            lore.add("§bhttps://flexmine.ru");
            itemMeta.setLore(lore);
            book.setItemMeta(itemMeta);
            inv.setItem(2 * 9 + 5 - 1, book);
        } else {
            lore.add("§fЧтобы выдать привилегию");
            lore.add("§eНажми §fна нее в меню");
            lore.add("§fИ §eнапиши в чат §fник игрока,");
            lore.add("§fКоторому хочешь выдать привилегию");
            itemMeta.setLore(lore);
            book.setItemMeta(itemMeta);
            inv.setItem(9 + 5 - 1, book);
        }
    }

    private Map<String, Integer> allowGiveGroup(Player player, Map<String, Integer> groups) {
        Map<String, Integer> playerGroups = Grant.getGiveGroups().get(Grant.getPermissions().getPrimaryGroup(player));
        Map<String, Integer> allowGroups = new HashMap<>();

        if (groups == null)
            return null;

        playerGroups.forEach((group, number) -> {
            if (groups.containsKey(group)) {
                if (number > groups.get(group))
                    allowGroups.put(group, number - groups.get(group));
            } else if (number > 0)
                allowGroups.put(group, groups.getOrDefault(group, number));
        });

        return allowGroups;
    }

    private ItemStack createItem(Material material, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        List<String> loreList = new ArrayList<>();
        Collections.addAll(loreList, lore);
        itemMeta.setLore(loreList);
        item.setItemMeta(itemMeta);
        return item;
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }

    public Map<String, Integer> getAllowGiveGroup() {
        return allowGiveGroup;
    }

    public Map<Integer, String> getInventoryGiveGroup() {
        return inventoryGiveGroup;
    }
}
