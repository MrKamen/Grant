package me.mrkamen.grant;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.sql.SQLException;

public class GrantInfoCommand implements CommandExecutor {
    private SQLConnection sqlConnection;

    public GrantInfoCommand(SQLConnection sqlConnection) {
        this.sqlConnection = sqlConnection;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("grant.info")) {
            sender.sendMessage("Недостаточно прав :(");
            return false;
        }
        if (args.length != 1) {
            sender.sendMessage("Используйте: /graninfo ник");
            return false;
        }
        try {
            sqlConnection.getPlayerInfo(sender, args[0]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
