package me.mrkamen.grant;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;

public class GrantCommand implements CommandExecutor {
    private SQLConnection sqlConnection;

    public GrantCommand(SQLConnection sqlConnection) {
        this.sqlConnection = sqlConnection;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Использовать /grant можно только через консоль");
            return false;
        }

        if (args.length > 0) {
            sender.sendMessage("Используйте команду §a/grant §cбез аргументов");
        }

        Player player = (Player) sender;
        if (!Grant.getGiveGroups().containsKey(Grant.getPermissions().getPrimaryGroup(player)))
            player.openInventory(new GrantInventory(player, null).getInventory());
        else {
            try {
                sqlConnection.getPlayerGroups(player);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
