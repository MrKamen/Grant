package me.mrkamen.grant;

import org.bukkit.Material;

public class Group {
    private String name;
    private Material material;
    private int priority;

    public Group(String name, String material, int priority) {
        this.name = name;
        this.material = Material.getMaterial(material);
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public Material getMaterial() {
        return material;
    }

    public int getPriority() {
        return priority;
    }
}
