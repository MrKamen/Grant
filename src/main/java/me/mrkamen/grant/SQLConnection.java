package me.mrkamen.grant;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class SQLConnection {
    private Grant grant;
    private Connection connection;
    private String url, user, password;

    public SQLConnection(String host, String user, String password, String database, int port, Grant grant) throws SQLException {
        this.grant = grant;
        this.user = user;
        this.password = password;
        this.url = "JDBC:mysql://" + host + ":" + port + "/" + database + "?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC";
    }

    public void saveGivePlayerGroup(String issuingPlayer, String receivingPlayer, String groupName) throws SQLException {
        String sql = "INSERT INTO grant_logs(`issuing_player`, `receiving_player`, `group_name`) " +
                "VALUES(?, ?, ?)";


        PreparedStatement p = getConnection().prepareStatement(sql);
        p.setString(1, issuingPlayer.toLowerCase());
        p.setString(2, receivingPlayer.toLowerCase());
        p.setString(3, groupName);
        Bukkit.getScheduler().runTaskAsynchronously(grant, () -> {
            try {
                p.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }


    public void getPlayerGroups(Player player) throws SQLException {
        String sql = "select * from `grant_logs` where issuing_player = ?";

        Connection c = getConnection();
        PreparedStatement p = c.prepareStatement(sql);
        p.setString(1, player.getName().toLowerCase());

        Bukkit.getScheduler().runTaskAsynchronously(grant, () -> {
            try {
                ResultSet result = p.executeQuery();
                Map<String, Integer> groups = new HashMap<>();

                while (result.next()) {
                    String group = result.getString("group_name");
                    if (groups.containsKey(group))
                        groups.put(group, groups.get(group) + 1);
                    else {
                        groups.put(group, 1);
                    }
                }
                GrantInventory grantInventory = new GrantInventory(player, groups);
                player.openInventory(grantInventory.getInventory());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void getPlayerInfo(CommandSender sender, String player) throws SQLException {
        String sql = "select * from `grant_logs` where `issuing_player` = ? OR `receiving_player` = ?";

        Connection c = getConnection();
        PreparedStatement p = c.prepareStatement(sql);
        p.setString(1, player.toLowerCase());
        p.setString(2, player.toLowerCase());

        Bukkit.getScheduler().runTaskAsynchronously(grant, () -> {
            try {
                ResultSet result = p.executeQuery();
                if (!result.next()) {
                    sender.sendMessage("Информации о игроке " + player + " отсутствует");
                } else {
                    do {
                        sender.sendMessage("Игрок " + result.getString("issuing_player")
                                + " выдал игроку " + result.getString("receiving_player")
                                + " группу " + result.getString("group_name") + " дата "
                                + result.getString("date")
                        );
                    }
                    while (result.next());
                }
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
            }
        });
    }

    private Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(url, user, password);
        }

        return connection;
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Throwable t) {
            Bukkit.getLogger().log(Level.SEVERE, "An error has occurred while closing the connection: ", t);
        }
    }

}